import pyspark
import sys

threshold = int(sys.argv[1])
outputFile = sys.argv[2]
item_num = 10000


def getfactor(num):
    factor = []
    for i in range(1, num + 1):
        if num % i == 0:
            factor.append(i)
    return factor


bucket = []
for i in range(1, item_num):
    bucket.append(getfactor(i))


def rm0(a):
    for item in a[:3]:
        if type(item) == int and item == 0:
            a.remove(item)
    return a


def aprior(frequent):
    result = [frequent, 0]
    for item in bucket:
        if type(frequent) == int:
            frequentset = set([frequent])
        else:
            frequentset = set(frequent)
        if frequentset.issubset(item):
            result[1] += 1
    return result


def newcutof(x, y):
    if y[1] > threshold:
        x += [y[0]]
    return x


def writeFile(file, outputList):
    with open(file, "a") as f:
        f.write(str(outputList) + '\n')


conf = pyspark.SparkConf().setAppName("myapp").setMaster("local")
sc = pyspark.SparkContext(conf=conf)

final = rm0(sc.parallelize(range(0, item_num), 1).map(aprior).reduce(newcutof))


i = 2
while True:
    next_round = [0]
    if i == 2:
        for val1 in range(0, len(final)):
            for val2 in range(val1 + 1, len(final)):
                if final[val1] != 0 and final[val2] != 0:
                    tup = (final[val1], final[val2])
                    next_round.append(tup)
    else:
        for val1 in range(0, len(final)):
            for val2 in range(val1 + 1, len(final)):
                verifyset = set(final[val1] + final[val2])
                if len(verifyset) == i:
                    next_round.append(tuple(verifyset))
        next_round = list(set(next_round))

    final = rm0(sc.parallelize(next_round, 1).map(aprior).reduce(newcutof))
    i += 1

    if len(final) == 0:
        break

    writeFile(outputFile, final)
